async function fetchcomputers() {
  try {
    const response = await fetch(
     //"https://dce-noroff-api.herokuapp.com/computers"
      "https://noroff-komputer-store-api.herokuapp.com/computers" 

    );
    const json = await response.json(); // JavaScript Object Notation
    return json; //  computers
  } catch (error) {
    console.error("Error: ", error.message);
  }



try {
  const get = await fetch(
   //"https://dce-noroff-api.herokuapp.com/computers"
    "https://noroff-komputer-store-api.herokuapp.com/" 

  );
  const json = await get.json(); // JavaScript Object Notation
  return json; //  computers
} catch (error) {
  console.error("Error: ", error.message);
}
}


// DOM Elements
const computersSelectElement = document.getElementById("computers");
const computerTitleElement = document.getElementById("computer-title");
const computerImageElement = document.getElementById("computer-image");
const computerspecsElement = document.getElementById("computer-specs");
const computerPriceElement = document.getElementById("computer-price");



// Can use await inside a script marked as type=module
const computers = await fetchcomputers();

for (const computer of computers) {
  // Add to the select.
  const html = `<option value="${computer.id}">${computer.title}</option>`;
  computersSelectElement.insertAdjacentHTML("beforeend", html);
}

/** @type HTMLSelectElement */
function onSelectChange() {
  const computerId = this.value;
  const computer = computers.find((computer) => computer.id === Number(computerId));
  renderSelectedcomputer(computer);
}




computersSelectElement.addEventListener("change", onSelectChange);
 // Rendering 
const renderSelectedcomputer = (computer) => {
  computerTitleElement.innerText = computer.title 
  computerPriceElement.innerText = "$" + computer.price 
 computerImageElement.src = computer.image;
  computerspecsElement.innerHTML = "";
  for (const key in computer.specs) {
    const spec = computer.specs[key];
    computerspecsElement.insertAdjacentHTML(
      "beforeend",
      `<li>${spec}</li>`
    );
  }

};

